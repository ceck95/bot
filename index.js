const google = require('googleapis');
const OAuth2 = google.auth.OAuth2;
const BPromise = require('bluebird');

const oauth2Client = new OAuth2(
  "814634091092-e30snfbkhlpfujj3nao5d70c4asjmg2k.apps.googleusercontent.com",
  "J2ClwZFHvUYaUIEWH15M0ySw",
  "http://nhutuit.com"
);

// generate a url that asks permissions for Google+ and Google Calendar scopes
const scopes = [
  'https://mail.google.com/'
];

/**
 * Generator url redirect get code
 */
// const url = oauth2Client.generateAuthUrl({
//   // 'online' (default) or 'offline' (gets refresh_token)
//   access_type: 'offline',

//   // If you only need one scope you can pass it as a string
//   scope: scopes,
//   approval_prompt: 'force'
//   // Optional property that passes state parameters to redirect URI
//   // state: { foo: 'bar' }
// });

const code = '4/AtgWV5jmunHIasPBP6gkJwc9eedQkj8PseZOaaa4gm0#';

/**
 * Function get token then get code
 */
// oauth2Client.getToken(code, function(err, tokens) {
//   // Now tokens contains an access_token and an optional refresh_token. Save them.
//   if (!err) {
//     oauth2Client.setCredentials(tokens);
//   }
//   console.error(err);
//   console.log(tokens);
// });


/**
 * Refresh token 
 */
oauth2Client.setCredentials({
  access_token: 'ya29.GluBBMYZNNLwS2NeNnSA3NPctAJ4CtliM3fyglC_UHLkI1Q5bqBmFrvLSMaOR1etFu_WzAq_wYdnc3xO-cmUK4RJeospkuSNZ0h56YnTCZ-uk6Vks8Wa8ZI22iCT',
  refresh_token: '1/A7lgEg3Pwf2TFvZKBNmwLWtl5IRA6A5vmRnTavihiDj1u2xb5XhpjOBGmKQ8671d'
});


const gmail = google.gmail('v1');

const funcHandleEmail = () => {
  return gmail.users.messages.list({
    auth: oauth2Client,
    userId: 'me',
    includeSpamTrash: true
  }, (err, response) => {

    if (err) {
      console.err(err);
      return null;
    }

    const viewDetailMessageFunc = (id) => {
      return new BPromise((resolve, reject) => {
        gmail.users.messages.get({
          auth: oauth2Client,
          userId: 'me',
          includeSpamTrash: true,
          id: id
        }, (err, response) => {
          if (err) {
            console.log(err);
            return reject(err);
          }
          const getKeyFromHeader = (key) => {
            const headers = response.payload.headers;
            let valueOfKey;
            headers.every(e => {
              if (e.name === key) {
                valueOfKey = e.value
                return false;
              }
              return true;
            });

            return valueOfKey;
          };
          const subject = getKeyFromHeader('Subject'),
            from = getKeyFromHeader('From'),
            to = getKeyFromHeader('To'),
            labelIds = response.labelIds;
          console.log('_________________________________________________');
          console.log(`Label Id: ${labelIds}`);
          console.log(`Subject:${subject}`)
          console.log(`From:${from}`)
          console.log(`To:${to}`)
          console.log('_________________________________________________');

          return resolve({
            id: id,
            labelIds: labelIds,
            subject: subject,
            from: from,
            to: to
          });
        });
      });
    };

    response.messages = response.messages.map(e => {
      return viewDetailMessageFunc(e.id);
    });

    return BPromise.all(response.messages).then((results) => {
      console.log('DELETING ....');
      const deleteEmail = (id) => {
        return new BPromise((resolve, reject) => {
          gmail.users.messages.delete({
            auth: oauth2Client,
            userId: 'me',
            id: id
          }, (err, response) => {
            if (err) {
              console.log(err);
              return resolve({
                id: id,
                messages: 'Delete not successfully'
              });
            }
            return resolve({
              id: id,
              messages: 'Delete successfully'
            });
          });
        });
      };

      results = results.filter(e => {
        if (e.labelIds) {
          return true;
        }
        return false;
      });

      const deleteEmailOfLabel = (label) => {
        const listIdOfLabel = results.filter(e => {
          if (e.labelIds.indexOf(label) >= 0) {
            return true;
          }
          return false;
        }).map(e => {
          return deleteEmail(e.id);
        });

        console.log(`EMAIL have ${label} ${listIdOfLabel.length}`)
        return listIdOfLabel;
      };

      const listPromiseCategoryUpdates = deleteEmailOfLabel('CATEGORY_UPDATES');
      const listPromiseSpam = deleteEmailOfLabel('SPAM');
      const listPromiseTrash = deleteEmailOfLabel('TRASH');
      const listPromiseCategoryPromotions = deleteEmailOfLabel('CATEGORY_PROMOTIONS');
      const listPromiseCategorySocial = deleteEmailOfLabel('CATEGORY_SOCIAL');
      const listPromiseLabel9 = deleteEmailOfLabel('Label_9');
      const listPromise = listPromiseSpam
        .concat(listPromiseTrash)
        .concat(listPromiseCategoryUpdates)
        .concat(listPromiseCategoryPromotions)
        .concat(listPromiseCategorySocial)
        .concat(listPromiseLabel9);

      return BPromise.all(listPromise).then(resultDelete => {
        console.log(`Analytics: `);
        console.log(`DELETE EMAIL SPAM: ${listPromiseSpam.length}`)
        console.log(`DELETE EMAIL TRASH: ${listPromiseTrash.length}`)
        console.log(`DELETE EMAIL CATEGORY_UPDATES: ${listPromiseCategoryUpdates.length}`)
        console.log(`DELETE EMAIL CATEGORY_PROMOTIONS: ${listPromiseCategoryPromotions.length}`)
        console.log(`DELETE EMAIL CATEGORY_SOCIAL: ${listPromiseCategorySocial.length}`)
        console.log(`DELETE EMAIL Label_9: ${listPromiseLabel9.length}`)
        const total = listPromiseSpam.length +
          listPromiseTrash.length +
          listPromiseCategoryUpdates.length +
          listPromiseCategoryPromotions.length +
          listPromiseLabel9.length +
          listPromiseCategorySocial.length;
        if (total > 0) {
          console.log('Continue .....');
          return funcHandleEmail();
        }
      }).catch(err => {
        console.error(err);
        return null;
      })

    }).catch(err => {
      console.error(err);
      return null;
    });

  });

};
console.log('BOT CLEAN MAIL BY NHUTDEV');

const type = process.argv[2];
if (type === 'clean') {
  funcHandleEmail();
} else {
  const CronJob = require('cron').CronJob;

  const job = new CronJob({
    cronTime: '00 0 0 * * *',
    onTick: () => {
      return funcHandleEmail();
    },
    start: true,
    timeZone: 'Asia/Ho_Chi_Minh'
  });
}